# dns

Dies ist ein einfacher resolving DNS-Server, der auf bind9 basiert.
Wenn der Container gebaut wird, werden alle Domains im Cache-Domain Repo also Forward Only Zonen eingetragen.

Der Cluster-Stack verwendet OSPF, um dem Core-Switch eine Cluster-IP mitzuteilen.
Siehe: https://gitlab.com/netsession1/dns/frr-ospf-bind9-cluster



## Docker Compose FRR OSPF Cluster 

```
---

services:
  frr-cluster:
    image: registry.gitlab.com/netsession1/dns/frr-ospf-bind9-cluster/cluster-main
    restart: always
    privileged: true
    network_mode: "host"
    depends_on: 
      - bind9
    environment:
      - CLUSTER_PRI_IP=10.255.255.10
      - CLUSTER_SEC_IP=10.255.255.11
  bind9:
    image: registry.gitlab.com/netsession1/dns/bind9-docker/bind9-main:latest
    stdin_open: true
    tty: true
    restart: always
    ports:
      - 53:53/udp
      - 53:53/tcp
    environment:
      - EXTRA_ARGS=-4
      - LANCACHE_IP=172.100.10.1
      - LOCALDNS_IP=172.100.10.1
    volumes:
      - var:/var/lib/bind
      - cache:/var/cache/bind
      
volumes:
  var: {}
  cache: {}

```



## Docker Compose Only DNS

```
---

  bind9:
    image: registry.gitlab.com/netsession1/dns/bind9-docker/bind9-main:latest
    stdin_open: true
    tty: true
    restart: always
    ports:
      - 53:53/udp
      - 53:53/tcp
    environment:
      - EXTRA_ARGS=-4
      - LANCACHE_IP=172.100.10.1
      - LOCALDNS_IP=172.100.10.1
    volumes:
      - var:/var/lib/bind
      - cache:/var/cache/bind
      
volumes:
  var: {}
  cache: {}

```