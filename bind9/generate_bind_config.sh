#!/bin/bash


BIND_DIR="/etc/bind"

if [ -z "$LOCALDNS_IP" ]; then
  echo "Die Umgebungsvariable LOCALDNS_IP ist nicht gesetzt."
  exit 1
fi

cd /opt/cache-domains

git pull

echo "" > $BIND_DIR/named.conf.extra-lanchache
echo "validate-except" > $BIND_DIR/named.conf.validate-except
echo "{" >> $BIND_DIR/named.conf.validate-except

declare -A domain_added

find . -type f -name '*.txt' > /tmp/domain_files.txt
while read file; do
    while read domain; do

        if [[ "$domain" =~ ^# ]]; then
            continue
        fi

        clean_domain=$(echo "$domain" | sed 's/^[*\.]*//')

        if [ -n "$clean_domain" ] && [ -z "${domain_added[$clean_domain]}" ]; then
            echo "Füge hinzu: $clean_domain"
            echo "zone \"$clean_domain\" {" >> $BIND_DIR/named.conf.extra-lanchache
            echo "    type forward;" >> $BIND_DIR/named.conf.extra-lanchache
            echo "    forward only;" >> $BIND_DIR/named.conf.extra-lanchache
            echo "    forwarders { $LANCACHE_IP; };" >> $BIND_DIR/named.conf.extra-lanchache
            echo "};" >> $BIND_DIR/named.conf.extra-lanchache
            echo "" >> $BIND_DIR/named.conf.extra-lanchache
            domain_added["$clean_domain"]=1
            echo "    \"$clean_domain\";" >> $BIND_DIR/named.conf.validate-except
        fi
    done < "$file"
done < /tmp/domain_files.txt

grep 'zone "' $BIND_DIR/named.conf.extra-zones | sed -E 's/zone "([^"]+)".*/\1/' | while read zone_domain; do
    if [ -z "${domain_added[$zone_domain]}" ]; then
        echo "    \"$zone_domain\";" >> $BIND_DIR/named.conf.validate-except
        domain_added["$zone_domain"]=1
    fi
done

echo "};" >> $BIND_DIR/named.conf.validate-except

sed -i "s/\$LOCALDNS_IP/$LOCALDNS_IP/g" $BIND_DIR/named.conf.extra-zones

# named-checkconf -z $BIND_DIR/named.conf

# exec /usr/sbin/named -g $EXTRA_ARGS
