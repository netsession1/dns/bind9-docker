FROM ubuntu/bind9:9.18-22.04_beta

RUN apt-get update && apt-get install -y \
    dnsutils \
    git \
    bash \
    && rm -rf /var/lib/apt/lists/*

WORKDIR /opt
RUN git clone https://github.com/uklans/cache-domains.git

COPY config/* /etc/bind/

COPY generate_bind_config.sh /usr/local/bin/
RUN chmod +x /usr/local/bin/generate_bind_config.sh

ENV EXTRA_ARGS=-4
ENV LANCACHE_IP=10.248.0.173
ENV LOCALDNS_IP=172.16.0.1

RUN /usr/local/bin/generate_bind_config.sh

HEALTHCHECK --interval=60s --timeout=4s CMD dig 1-ttl.clair.cloud @127.0.0.1 | grep 205.234.117.58 || exit 1

# ENTRYPOINT ["/usr/local/bin/generate_bind_config.sh"]

RUN named-checkconf -z /etc/bind/named.conf

# CMD ["/bin/bash"]