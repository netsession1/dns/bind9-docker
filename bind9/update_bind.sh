#!/bin/bash

BIND_DIR="/etc/bind"
LOCAL_REPO_DIR="/opt/cache-domains"
REPO_URL="https://github.com/uklans/cache-domains.git"

# Funktion, um das Repository zu klonen
clone_repo() {
    if [ -d "$LOCAL_REPO_DIR" ]; then
        rm -rf "$LOCAL_REPO_DIR"
    fi
    git clone "$REPO_URL" "$LOCAL_REPO_DIR"
}

# Funktion, um die Domain-Liste zu parsen
parse_domains() {
    domains=()
    while IFS= read -r line; do
        line=$(echo "$line" | sed 's/^[*\.]*//')
        if [[ ! "$line" =~ ^# ]] && [[ -n "$line" ]]; then
            domains+=("$line")
        fi
    done < <(find "$LOCAL_REPO_DIR" -type f -name '*.txt' -exec cat {} +)
    echo "${domains[@]}"
}

# Funktion, um eine Zonendatei zu erstellen
create_zone_file() {
    local domain="$1"
    local subdomains="$2"
    local zone_file="$BIND_DIR/db.$domain"
    
    echo "\$TTL 86400" > "$zone_file"
    echo "@   IN  SOA     localhost. root.localhost. (" >> "$zone_file"
    echo "                $(date +%Y%m%d%H)  ; Serial" >> "$zone_file"
    echo "                3600        ; Refresh" >> "$zone_file"
    echo "                1800        ; Retry" >> "$zone_file"
    echo "                1209600     ; Expire" >> "$zone_file"
    echo "                86400 )     ; Minimum TTL" >> "$zone_file"
    echo ";" >> "$zone_file"
    echo "@       IN  NS      localhost." >> "$zone_file"
    echo "@       IN  NS      127.0.0.1" >> "$zone_file"
    echo "@       IN  A       $LANCACHE_IP" >> "$zone_file"

    for subdomain in $subdomains; do
        echo "$subdomain. IN A $LANCACHE_IP" >> "$zone_file"
    done

    echo "Created zone file: $zone_file"
}

# Hauptfunktion
main() {
    if [ -z "$LANCACHE_IP" ]; then
        echo "Die Umgebungsvariable LANCACHE_IP ist nicht gesetzt."
        exit 1
    fi

    clone_repo
    all_domains=$(parse_domains)
    declare -A zone_domains

    for domain in $all_domains; do
        main_domain=$(echo "$domain" | awk -F'.' '{print $(NF-1)"."$NF}')
        sub_domain=${domain%.${main_domain}}

        if [ -z "$sub_domain" ]; then
            sub_domain="@"
        fi

        zone_domains["$main_domain"]+="$sub_domain "
    done

    for zone in "${!zone_domains[@]}"; do
        create_zone_file "$zone" "${zone_domains[$zone]}"
    done

    named-checkconf -z "$BIND_DIR/named.conf"
}

main
